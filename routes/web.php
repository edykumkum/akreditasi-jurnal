<?php

// Penilaian Jalur 1
Route::get('/', function(){ return view('index');});
Route::post('/penilaian-1', 'PenilaianAkreditasiController@proses_penilaian_jalur1');
Route::post('/penilaian-2', 'PenilaianAkreditasiController@proses_penilaian_jalur1');
Route::post('/penilaian-3', 'PenilaianAkreditasiController@proses_penilaian_jalur1');
Route::post('/penilaian-4', 'PenilaianAkreditasiController@proses_penilaian_jalur1');
Route::post('/penilaian-5', 'PenilaianAkreditasiController@proses_penilaian_jalur1');
Route::post('/penilaian-6', 'PenilaianAkreditasiController@proses_penilaian_jalur1');
Route::post('/penilaian-7', 'PenilaianAkreditasiController@proses_penilaian_jalur1');
Route::post('/penilaian-8', 'PenilaianAkreditasiController@proses_penilaian_jalur1');
Route::post('/validasi-nilai', 'PenilaianAkreditasiController@validasi_nilai');
Route::post('/proses-penilaian', 'PenilaianAkreditasiController@menilai');

// Riwayat Penilaian
Route::group(['middleware' => 'auth'], function (){
    Route::get('/history-penilaian', function(){ return view('history-nilai');});
    Route::get('/detail-history-penilaian/{id}', 'PenilaianAkreditasiController@detail_history');
});

// Passing Data ke Datatable
Route::get('getdatahistory', ['as'=>'datatable.gethistorynilai','uses'=>'GetHistoryController@history']);

// Penilaian Jalur 2
Route::get('/penilaian1/{token}/{id}/{idunsurpenilaian}', 'PenilaianLanjutanController@proses_penilaian_jalur2');
Route::get('/penilaian2/{token}/{id}/{idunsurpenilaian}', 'PenilaianLanjutanController@proses_penilaian_jalur2');
Route::get('/penilaian3/{token}/{id}/{idunsurpenilaian}', 'PenilaianLanjutanController@proses_penilaian_jalur2');
Route::get('/penilaian4/{token}/{id}/{idunsurpenilaian}', 'PenilaianLanjutanController@proses_penilaian_jalur2');
Route::get('/penilaian5/{token}/{id}/{idunsurpenilaian}', 'PenilaianLanjutanController@proses_penilaian_jalur2');
Route::get('/penilaian6/{token}/{id}/{idunsurpenilaian}', 'PenilaianLanjutanController@proses_penilaian_jalur2');
Route::get('/penilaian7/{token}/{id}/{idunsurpenilaian}', 'PenilaianLanjutanController@proses_penilaian_jalur2');
Route::get('/penilaian8/{token}/{id}/{idunsurpenilaian}', 'PenilaianLanjutanController@proses_penilaian_jalur2');

// Lanjut Proses Penilaian Jalur 2
Route::post('/penilaian-1-lanjutan', 'PenilaianLanjutanController@proses_lanjutan_penilaian');
Route::post('/penilaian-2-lanjutan', 'PenilaianLanjutanController@proses_lanjutan_penilaian');
Route::post('/penilaian-3-lanjutan', 'PenilaianLanjutanController@proses_lanjutan_penilaian');
Route::post('/penilaian-4-lanjutan', 'PenilaianLanjutanController@proses_lanjutan_penilaian');
Route::post('/penilaian-5-lanjutan', 'PenilaianLanjutanController@proses_lanjutan_penilaian');
Route::post('/penilaian-6-lanjutan', 'PenilaianLanjutanController@proses_lanjutan_penilaian');
Route::post('/penilaian-7-lanjutan', 'PenilaianLanjutanController@proses_lanjutan_penilaian');
Route::post('/penilaian-8-lanjutan', 'PenilaianLanjutanController@proses_lanjutan_penilaian');

// Ubah Nilai
Route::post('/update', 'UbahNilaiController@ubah_penilaian_sementara');

Auth::routes();
Route::get('/login-admin', 'HomeController@index');
Route::get('/regxuser', function(){ return view('reguser');});
