<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Datatables;

class GetHistoryController extends Controller
{
    public function history()
    {
        $history=DB::table('tb_penilaian')->get();
        // $tes = $history->id_penilaian;
        // echo $tes;
        return Datatables::of($history)
        ->addColumn('actions', function($history) {
        return '<a href="detail-history-penilaian/'.$history->id_penilaian.'">Lihat</a>';
        })
        ->make(true);
    }
}
