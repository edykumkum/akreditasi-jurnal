<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mail;

class PenilaianLanjutanController extends Controller
{
    public function proses_penilaian_jalur2($token, $id, $idunsurpenilaian){
        $detail=DB::table('tb_penilaian_sementara')
        ->select('*')
        ->where('id_nilai_sementara', '=', $id)
        ->where('token', '=', $token)
        ->first();

        $judulpaper       = $detail->judul_paper;
        $pengelola        = $detail->pengelola;
        $email            = $detail->email;
        $nilai_sementara  = $detail->nilai_sementara;

        $array_nilai      = json_decode($nilai_sementara, true);
        $id_unsur_nilai   = $idunsurpenilaian - 1;
        $total_nilai      = $array_nilai[$id_unsur_nilai]['total_nilai'];

        $arrayidindikator = array();

        if($idunsurpenilaian == 1 || $idunsurpenilaian == 2){
            $checkedradiobutton=DB::table('tb_indikator_penilaian')
                ->select('*')
                ->where('id_unsur_penilaian', '=', $idunsurpenilaian)
                ->where('nilai', '=', $total_nilai)
                ->get();
            $arrayidindikator[]=$checkedradiobutton;
        }

        elseif($idunsurpenilaian == 3 || $idunsurpenilaian == 4 || $idunsurpenilaian == 5 || $idunsurpenilaian == 6 || $idunsurpenilaian == 7 || $idunsurpenilaian == 8){
            $idsub  = $array_nilai[$id_unsur_nilai]['id_sub'];
            $nilai = $array_nilai[$id_unsur_nilai]['Nilai'];
            foreach (array_combine ($idsub, $nilai) as $tes1 => $tes2) {
            $checkedradiobutton=DB::table('tb_indikator_penilaian')
                ->select('*')
                ->where('id_sub_unsur_penilaian', '=', $tes1)
                ->where('nilai', '=', $tes2)
                ->get();
                $arrayidindikator[]=$checkedradiobutton;
            }
        }
        $arrayterakhir = array_values(array_slice($array_nilai, -1))[0]['id_penilaian'];
        $array_idindikator = collect($arrayidindikator); // echo $array_idindikator;
        $json_idindikator = $array_idindikator->toJson();
        $id_indikator = json_decode($json_idindikator, true); // echo $id_indikator[0][0]['id_indikator'];

        $penilaian=DB::table('tb_unsur_penilaian')
            ->where('id_unsur', '=', $idunsurpenilaian)
            ->first();

        $record_terakhir = $arrayterakhir;
        $total_skor_baru = $total_nilai;
        $array_penilaians = $nilai_sementara;
        $nomor_halaman = $idunsurpenilaian;
        $isi = DB::table('tb_indikator_penilaian')
            ->leftJoin('tb_sub_unsur_penilaian', 'tb_sub_unsur_penilaian.id_sub_unsur', '=', 'tb_indikator_penilaian.id_sub_unsur_penilaian')
            ->leftJoin('tb_unsur_penilaian', 'tb_indikator_penilaian.id_unsur_penilaian', '=', 'tb_unsur_penilaian.id_unsur')
            ->select('tb_indikator_penilaian.id_indikator', 'tb_indikator_penilaian.indikator', 'tb_sub_unsur_penilaian.sub_unsur_penilaian', 'tb_unsur_penilaian.unsur_penilaian', 'tb_indikator_penilaian.nilai', 'tb_indikator_penilaian.id_sub_unsur_penilaian', 'tb_indikator_penilaian.id_unsur_penilaian')
            ->where('tb_indikator_penilaian.id_unsur_penilaian', '=', $idunsurpenilaian)
            ->get();

        return view('penilaian-'.$nomor_halaman.'-lanjutan', compact('penilaian', 'isi', 'judulpaper', 'pengelola', 'email', 'total_skor_baru', 'id_indikator', 'token', 'id', 'idunsurpenilaian', 'arrayterakhir', 'array_penilaians', 'record_terakhir'));
    }

    public function proses_lanjutan_penilaian(Request $request){
        $token           = $request->input('token');
        $id              = $request->input('idnye');
        $judulpaper      = $request->input('judulpaper');
        $pengelola       = $request->input('pengelola');
        $email           = $request->input('email');
        $halaman         = $request->input('nopage');
        $total_skor_lama = $request->input('total_skor_lama');
        $arraynilai      = array();

        $arrayterakhir = $halaman + 1;

        $record_terakhir = $_POST['record_terakhir'];

        if ($halaman == 1) {
            $idunsur = $_POST['idunsur_penilaian'];
            if(isset($_POST['indikator'])) {
                $total_skor = 0;
                foreach($_POST['indikator'] as $name => $value) {
                    $total_skor+= $value;
                }
                $total_skor_baru = $total_skor;
            }
            $row_array_detail['id_penilaian']=$idunsur;
            $row_array_detail['total_nilai']=$total_skor_baru;
            array_push($arraynilai, $row_array_detail);

            $array_penilaians = json_encode($arraynilai);
            // print_r($array_penilaians);
        }

        elseif ($halaman == 2) {
            $idunsur         = $_POST['idunsur_penilaian'];
            $array_penilaian = $_POST['array_penilaian'];
            if($record_terakhir != 2){
                if(isset($_POST['indikator'])) {
                    $total_skor = 0;
                    foreach($_POST['indikator'] as $name => $value) {
                        $total_skor+= $value;
                    }
                    $total_skor_baru = $total_skor + $total_skor_lama;
                }
                $row_array_detail['id_penilaian']=$idunsur;
                $row_array_detail['total_nilai']=$total_skor;
                array_push($arraynilai, $row_array_detail);
                $array_penilaian_tambahan = json_encode($arraynilai);
                $array_penilaians=json_encode(array_merge(json_decode($array_penilaian, true),json_decode($array_penilaian_tambahan, true)));
                // print_r($array_penilaians);
            }
            elseif ($record_terakhir == 2) {
                $array_penilaians = $array_penilaian;
                $total_skor_baru = $total_skor_lama;
                // print_r($array_penilaians);
            }
        }

        elseif ($halaman == 3 || $halaman == 4 || $halaman == 5 || $halaman == 6 || $halaman == 7) {
            $idunsur         = $_POST['idunsur_penilaian'];
            $idsub           = $_POST['id_subunsur_penilaian'];
            $array_penilaian = $_POST['array_penilaian'];
            $nilai           = $_POST['indikator'];

            if(($record_terakhir > 0 && $record_terakhir < 9 ) && $halaman != $record_terakhir ) {
                  // echo $record_terakhir; die();
                  if(isset($_POST['indikator'])) {
                      $total_skor = 0;
                      foreach($_POST['indikator'] as $name => $value) {
                          $total_skor+= $value;
                      }
                      $total_skor_baru = $total_skor + $total_skor_lama;
                  }

                  $row_array_detail['id_penilaian']=$idunsur;
                  $row_array_detail['id_sub']=$idsub;
                  $row_array_detail['Nilai']=$nilai;
                  $row_array_detail['total_nilai']=$total_skor;
                  array_push($arraynilai, $row_array_detail);
                  $array_penilaian_tambahan = json_encode($arraynilai);

                  $array_penilaians = json_encode(array_merge(json_decode($array_penilaian, true),json_decode($array_penilaian_tambahan, true)));
                  // print_r($array_penilaians);
                  // $tes = json_decode($array_penilaians, true);
                  // echo $tes[0]['total_nilai'];

            }
            elseif($record_terakhir == 3 || $record_terakhir == 4 || $record_terakhir == 5 || $record_terakhir == 6 || $record_terakhir == 7 || $record_terakhir == 8){
                $array_penilaians = $array_penilaian;
                $total_skor_baru = $total_skor_lama;
                // print_r($array_penilaians);
            }
        }

        if (isset($_POST['simpan'])) {
            $token = $request->input('token');
            $idnye = $request->input('idnye');

            $updt = DB::table('tb_penilaian_sementara')
            ->where('token', '=', $token)
            ->where('id_nilai_sementara', '=', $idnye)
            ->update(['nilai_sementara' => $array_penilaians]);

            $penilaian=DB::table('tb_penilaian_sementara')
                ->where('token', '=', $token)
                ->first();
            $idnye = $penilaian->id_nilai_sementara;
            $idunsurpenilaian = $halaman;
            $urlnye= 'http://203.24.50.76/evaluasidiri/penilaian'.$halaman.'/'.$token.'/'.$idnye.'/'.$idunsurpenilaian;
            $data=['url'=>$urlnye];

            $emailpenerima = $request->input('email');

            Mail::send(['html'=>'mail'], $data, function($message) use($emailpenerima){
                // echo $emailpenerima;
                // $emailpenerima = 'edyseptiandri@gmail.com';
                $penerima ='Penginput Evaluasi Jurnal';

                $emailpengirim ='evaluasi.jurnal@gmail.com';
                $pengirim ='Evaluasi Jurnal';

                $message->to($emailpenerima, $penerima)->subject('Melanjutkan penilaian evaluasi jurnal');
                $message->from($emailpengirim, $pengirim);
            });
            return redirect('/');
        }

        if (isset($_POST['ubah'])) {
            $halaman = $request->input('nopage');
            $token = $request->input('token');
            $id = $request->input('idnye');
            $nilai = $_POST['indikator'];

            $detail=DB::table('tb_penilaian_sementara')
                ->select('*')
                ->where('id_nilai_sementara', '=', $id)
                ->where('token', '=', $token)
                ->first();

            $nilaisementara = $detail->nilai_sementara; // echo $nilaisementara;
            $array_nilai = json_decode($nilaisementara, true); // echo $array_nilai[$noarray]['total_nilai'];

            $sum = 0;
            foreach($_POST['indikator'] as $name => $value) {
                $sum+= $value;
            }

            if ($halaman == 1 || $halaman == 2) {
                $noarray = $halaman - 1;
                foreach($array_nilai as $key => $value)
                {
                    $array_nilai[$noarray]['total_nilai'] = $sum;
                }
            }

            else {
                $noarray = $halaman - 1;
                foreach($array_nilai as $key => $value)
                {
                    $array_nilai[$noarray]['Nilai'] = $nilai;
                    $array_nilai[$noarray]['total_nilai'] = $sum;
                }
            }
            $nilai_encode = json_encode($array_nilai);
            DB::table('tb_penilaian_sementara')
                ->where('id_nilai_sementara', $id)
                ->where('token', $token)
                ->update(['nilai_sementara' => $nilai_encode]);

            return redirect()->action(
                'PenilaianLanjutanController@proses_penilaian_jalur2', ['token'=>$token, 'id' => $id, 'idunsurpenilaian'=> $halaman]
            );
        }

        $id_unsur = $halaman + 1;
        $penilaian=DB::table('tb_unsur_penilaian')
            ->where('id_unsur', '=', $id_unsur)
            ->first();

        $id_indikator = 0;
        $page = $id_unsur;
        $isi = DB::table('tb_indikator_penilaian')
            ->leftJoin('tb_sub_unsur_penilaian', 'tb_sub_unsur_penilaian.id_sub_unsur', '=', 'tb_indikator_penilaian.id_sub_unsur_penilaian')
            ->leftJoin('tb_unsur_penilaian', 'tb_indikator_penilaian.id_unsur_penilaian', '=', 'tb_unsur_penilaian.id_unsur')
            ->select('tb_indikator_penilaian.indikator', 'tb_sub_unsur_penilaian.sub_unsur_penilaian', 'tb_unsur_penilaian.unsur_penilaian', 'tb_indikator_penilaian.nilai', 'tb_indikator_penilaian.id_sub_unsur_penilaian', 'tb_indikator_penilaian.id_unsur_penilaian')
            ->where('tb_indikator_penilaian.id_unsur_penilaian', '=', $id_unsur)
            ->get();
        return view('penilaian-'.$page.'-lanjutan', compact('judulpaper', 'pengelola', 'email', 'isi', 'penilaian', 'total_skor_baru', 'array_penilaians', 'arrayterakhir', 'token', 'id', 'id_indikator', 'record_terakhir'));
    }
}
