<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Requests;

use Mail;

class PenilaianAkreditasiController extends Controller
{
    public function proses_penilaian_jalur1(Request $request){
        $token           = $request->input('token');
        $id              = $request->input('idnye');
        $judulpaper      = $request->input('judulpaper');
        $pengelola       = $request->input('pengelola');
        $email           = $request->input('email');
        $halaman         = $request->input('nopage');
        $total_skor_lama = $request->input('total_skor_lama');
        $arraynilai      = array();

        if ($halaman == 1) {
            $idunsur = $_POST['idunsur_penilaian'];
            if(isset($_POST['indikator'])) {
                $total_skor = 0;
                foreach($_POST['indikator'] as $name => $value) {
                    $total_skor+= $value;
                }
                $total_skor_baru = $total_skor;
            }
            $row_array_detail['id_penilaian']=$idunsur;
            $row_array_detail['total_nilai']=$total_skor_baru;
            array_push($arraynilai, $row_array_detail);

            $array_penilaians = json_encode($arraynilai);
            // print_r($array_penilaians);
        }

        elseif ($halaman == 2) {
            $idunsur         = $_POST['idunsur_penilaian'];
            $array_penilaian = $_POST['array_penilaian'];

            if(isset($_POST['indikator'])) {
                $total_skor = 0;
                foreach($_POST['indikator'] as $name => $value) {
                    $total_skor+= $value;
                }
                $total_skor_baru = $total_skor + $total_skor_lama;
            }
            $row_array_detail['id_penilaian']=$idunsur;
            $row_array_detail['total_nilai']=$total_skor;
            array_push($arraynilai, $row_array_detail);
            $array_penilaian_tambahan = json_encode($arraynilai);
            $array_penilaians=json_encode(array_merge(json_decode($array_penilaian, true),json_decode($array_penilaian_tambahan, true)));
            // print_r($array_penilaians);
        }

        elseif ($halaman == 3 || $halaman == 4 || $halaman == 5 || $halaman == 6 || $halaman == 7) {
            $idunsur         = $_POST['idunsur_penilaian'];
            $idsub           = $_POST['id_subunsur_penilaian'];
            $array_penilaian = $_POST['array_penilaian'];
            $nilai           = $_POST['indikator'];

            if(isset($_POST['indikator'])) {
                $total_skor = 0;
                foreach($_POST['indikator'] as $name => $value) {
                    $total_skor+= $value;
                }
                $total_skor_baru = $total_skor + $total_skor_lama;
            }

            $row_array_detail['id_penilaian']=$idunsur;
            $row_array_detail['id_sub']=$idsub;
            $row_array_detail['Nilai']=$nilai;
            $row_array_detail['total_nilai']=$total_skor;
            array_push($arraynilai, $row_array_detail);
            $array_penilaian_tambahan = json_encode($arraynilai);

            $array_penilaians = json_encode(array_merge(json_decode($array_penilaian, true),json_decode($array_penilaian_tambahan, true)));
            // print_r($array_penilaians);
            // $tes = json_decode($array_penilaians, true);
            // echo $tes[0]['total_nilai'];
        }

        if (isset($_POST['simpan'])) {
            $tokenz =  md5($email.date('dmY').time().rand());
            DB::table('tb_penilaian_sementara')
            ->insert([
                'judul_paper' => $judulpaper,
                'pengelola' => $pengelola,
                'email' => $email,
                'nilai_sementara' => $array_penilaians,
                'token' => $tokenz
            ]);

            $penilaian=DB::table('tb_penilaian_sementara')
                ->where('token', '=', $tokenz)
                ->first();
            $idnye = $penilaian->id_nilai_sementara;
            $idunsurpenilaian = $halaman;
            $urlnye= 'http://localhost:8000/penilaian'.$halaman.'/'.$tokenz.'/'.$idnye.'/'.$idunsurpenilaian;
            $data=['url'=>$urlnye];

            $emailpenerima = $request->input('email');

            Mail::send(['html'=>'mail'], $data, function($message) use($emailpenerima){
                $penerima ='Penginput Evaluasi Jurnal';

                $emailpengirim ='evaluasi.jurnal@gmail.com';
                $pengirim ='Evaluasi Jurnal';

                $message->to($emailpenerima, $penerima)->subject('Melanjutkan penilaian evaluasi jurnal');
                $message->from($emailpengirim, $pengirim);
            });
            return redirect('/');
        }

        $id_unsur = $halaman + 1;
        $penilaian=DB::table('tb_unsur_penilaian')
            ->where('id_unsur', '=', $id_unsur)
            ->first();

        $page = $id_unsur;
        $isi = DB::table('tb_indikator_penilaian')
            ->leftJoin('tb_sub_unsur_penilaian', 'tb_sub_unsur_penilaian.id_sub_unsur', '=', 'tb_indikator_penilaian.id_sub_unsur_penilaian')
            ->leftJoin('tb_unsur_penilaian', 'tb_indikator_penilaian.id_unsur_penilaian', '=', 'tb_unsur_penilaian.id_unsur')
            ->select('tb_indikator_penilaian.indikator', 'tb_sub_unsur_penilaian.sub_unsur_penilaian', 'tb_unsur_penilaian.unsur_penilaian', 'tb_indikator_penilaian.nilai', 'tb_indikator_penilaian.id_sub_unsur_penilaian', 'tb_indikator_penilaian.id_unsur_penilaian')
            ->where('tb_indikator_penilaian.id_unsur_penilaian', '=', $id_unsur)
            ->get();
        return view('form-akreditasi-'.$page, compact('judulpaper', 'pengelola', 'email', 'isi', 'penilaian', 'total_skor_baru', 'array_penilaians'));
    }

    public function validasi_nilai(Request $request){
        $token = $request->input('token');
        $id = $request->input('idnye');
        $judulpaper = $request->input('judulpaper');
        $pengelola = $request->input('pengelola');
        $email = $request->input('email');

        $total_skor_lama = $request->input('total_skor_lama');

        $idunsur = $_POST['idunsur_penilaian'];
        $idsub = $_POST['id_subunsur_penilaian'];
        $nilai = $_POST['indikator'];
        $array7 = $_POST['array_penilaian'];

        $arraynilai = array();
        $row_array_detail['id_penilaian']=$idunsur;
        $row_array_detail['id_sub']=$idsub;
        $row_array_detail['Nilai']=$nilai;

        if(isset($_POST['indikator'])) {
            $total_skor = 0;
            foreach($_POST['indikator'] as $name => $value) {
                $total_skor+= $value;
            }
            $total_skor_baru = $total_skor + $total_skor_lama;
        }

        $row_array_detail['total_nilai']=$total_skor;
        array_push($arraynilai, $row_array_detail);
        $array_penilaian_tambahan = json_encode($arraynilai);

        $array_penilaian_akhir = json_encode(array_merge(json_decode($array7, true),json_decode($array_penilaian_tambahan, true)));
        $array_data_penilaian_akhir = json_decode($array_penilaian_akhir, true);

        $total = 0;
        for ($i=0; $i < 8; $i++) {
            $t = $array_data_penilaian_akhir[$i]['total_nilai'];
            $total+=$t;
        }
        // echo $total;
        return view('validasi-nilai', compact('judulpaper', 'pengelola', 'email', 'total', 'array_data_penilaian_akhir', 'array_penilaian_akhir', 'token', 'id'));
    }

    public function menilai(Request $request){
        $token = $request->input('token');
        $idnye = $request->input('idnye');
        $judulpaper = $request->input('judulpaper');
        $pengelola = $request->input('pengelola');
        $email = $request->input('email');
        $penilaian_akhir = $request->input('penilaian_akhir');

        DB::table('tb_penilaian')->insert([
          'judul_paper' => $judulpaper,
          'pengelola' => $pengelola,
          'email' => $email,
          'nilai' => $penilaian_akhir ]);

        $delete_penilaian_sementara = DB::table('tb_penilaian_sementara')
          ->where('token', '=', $token)
          ->where('id_nilai_sementara', '=', $idnye)
          ->delete();

        return redirect('/');
    }

    public function detail_history($id){
        $detail=DB::table('tb_penilaian')
        ->select('*')
        ->where('id_penilaian', '=', $id)
        ->first();

        $judul = $detail->judul_paper;
        $pengelola = $detail->pengelola;
        $email = $detail->email;
        $nilai = $detail->nilai;
        $detailnye = json_decode($nilai, true);
        // print_r($detailnye);
        // echo $detailnye[7]['total_nilai'];
        return view('detail-history', compact('judul', 'pengelola', 'email', 'detailnye'));
    }
}
