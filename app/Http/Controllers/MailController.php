<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Mail;

class MailController extends Controller
{
    // first, we create function for send Basics email
    public function url_email(){
        $urlnye= 'http://203.24.50.76/evaluasidiri';
        $data=['url'=>$urlnye];

        Mail::send(['text'=>'mail'], $data, function($message){

            $emailpenerima='edyseptiandri@gmail.com';
            $penerima='Penginput Evaluasi Jurnal';

            $emailpengirim='edyseptiandri@untan.ac.id';
            $pengirim='Evaluasi Jurnal';

            $message->to($emailpenerima, $penerima)->subject('Melanjutkan menilai evaluasi jurnal');
            $message->from($emailpengirim, $pengirim);
        });
        echo 'Url telah dikirim via Email!';
    }
}
