<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UbahNilaiController extends Controller
{
    public function ubah_penilaian_sementara(Request $request)
    {
        if (isset($_POST['ubah'])) {
            $halaman = $request->input('nopage');
            $token = $request->input('token');
            $id = $request->input('idnye');
            $nilai = $_POST['indikator'];

            $detail=DB::table('tb_penilaian_sementara')
                ->select('*')
                ->where('id_nilai_sementara', '=', $id)
                ->where('token', '=', $token)
                ->first();

            $nilaisementara = $detail->nilai_sementara; // echo $nilaisementara;
            $array_nilai = json_decode($nilaisementara, true); // echo $array_nilai[$noarray]['total_nilai'];

            $sum = 0;
            foreach($_POST['indikator'] as $name => $value) {
                $sum+= $value;
            }

            if ($halaman == 1 || $halaman == 2) {
                $noarray = $halaman - 1;
                foreach($array_nilai as $key => $value)
                {
                    $array_nilai[$noarray]['total_nilai'] = $sum;
                }
            }

            else {
                $noarray = $halaman - 1;
                foreach($array_nilai as $key => $value)
                {
                    $array_nilai[$noarray]['Nilai'] = $nilai;
                    $array_nilai[$noarray]['total_nilai'] = $sum;
                }
            }
            $nilai_encode = json_encode($array_nilai);
            DB::table('tb_penilaian_sementara')
                ->where('id_nilai_sementara', $id)
                ->where('token', $token)
                ->update(['nilai_sementara' => $nilai_encode]);

            return redirect()->action(
                'PenilaianLanjutanController@lanjut_input'.$halaman, ['token'=>$token, 'id' => $id]
            );
        }
    }
}
