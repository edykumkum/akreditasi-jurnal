-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 21, 2017 at 01:50 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `akreditasi_jurnal_untan`
--

-- --------------------------------------------------------

--
-- Table structure for table `materis`
--

CREATE TABLE `materis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2017_01_09_032027_bikin_table_materi', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('adminjurnal@gmail.com', 'd4d3f12d6704d66e4653581ab6610884c295b84ceac7b53b4a4c40094d8e0a9c', '2017-03-19 10:05:40');

-- --------------------------------------------------------

--
-- Table structure for table `tb_indikator_penilaian`
--

CREATE TABLE `tb_indikator_penilaian` (
  `id_indikator` int(11) NOT NULL,
  `id_unsur_penilaian` int(11) NOT NULL,
  `id_sub_unsur_penilaian` int(11) DEFAULT NULL,
  `indikator` varchar(200) NOT NULL,
  `nilai` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_indikator_penilaian`
--

INSERT INTO `tb_indikator_penilaian` (`id_indikator`, `id_unsur_penilaian`, `id_sub_unsur_penilaian`, `indikator`, `nilai`) VALUES
(1, 1, NULL, 'Spesifik sehingga mencerminkan superspesialisasi atau spesialisasi disiplin ilmu tertentu', 3),
(2, 1, NULL, 'Cukup spesifik tetapi meluas mencakup bidang ilmu', 2),
(3, 1, NULL, 'Kurang spesifik dan bersifat umum', 1),
(4, 1, NULL, 'Tidak spesifik dan/atau memakai nama lembaga/lokasi lokal', 0),
(5, 2, NULL, 'Organisasi profesi ilmiah', 4),
(6, 2, NULL, 'Organisasi profesi ilmiah bekerjasama dengan perguruan tinggi dan/atau lembaga penelitian dan pengembangan/Kementerian/Non Kementerian', 3),
(7, 2, NULL, 'Perguruan tinggi, lembaga penelitian dan pengembangan', 2),
(8, 2, NULL, 'Badan penerbitan non pemerintah atau perguruan tinggi yang mendelegasikan\r\nke sub kelembagaan di bawahnya', 1),
(9, 2, NULL, 'Penerbit selain a, b, c dan d', 0),
(10, 3, 1, 'Melibatkan mitra bebestari berkualifikasi internasional >50% dari beberapa negara', 5),
(11, 3, 1, 'Melibatkan mitra bebestari berkualifikasi nasional >50% dari berbagai institusi', 3),
(12, 3, 1, 'Melibatkan mitra bebestari setempat', 1),
(13, 3, 1, 'Tidak melibatkan mitra bebestari', 0),
(14, 3, 2, 'Baik sekali(Mitra bebestari ketat menjaring naskah, memberikan catatan dan saran perbaikan substansif sehingga kespesialisan naskah berkala terjaga)', 2),
(15, 3, 2, 'Baik (Mitra bebestari membantu menjaring naskah, memberikan catatan, dan data perbaikan seperlunya)', 1),
(16, 3, 2, 'Cukup Baik(Mitra bebestari kurang nyata dampak kinerjanya) ', 0),
(17, 3, 3, 'Lebih dari 50% penyunting sudah pernah menulis artikel di terbitan berkala ilmiah internasional', 3),
(18, 3, 3, 'Kurang dari 50% penyunting sudah pernah menulis artikel di terbitan berkala ilmiah internasional', 2),
(19, 3, 3, 'Lainnya yang belum berpengalaman menulis artikel di terbitan berkala ilmiah internasional', 1),
(20, 3, 4, 'Terinci, lengkap, jelas, sistematis dan tersedia contoh atau template ', 2),
(21, 3, 4, 'Kurang lengkap dan kurang jelas', 1),
(22, 3, 4, 'Tidak lengkap dan tidak jelas ', 0),
(23, 3, 5, 'Baik sekali dan sangat konsisten', 2),
(24, 3, 5, 'Baik dan konsisten', 1),
(25, 3, 5, 'Lainnya (tidak baik atau tidak konsisten) ', 0),
(26, 3, 6, 'Menggunakan manajemen pengelolaan penyuntingan secara daring penuh', 3),
(27, 3, 6, 'Menggunakan manajemen pengelolaan penyuntingan secara kombinasi daring dan surat elektronik', 2),
(28, 3, 6, 'Menggunakan manajemen pengelolaan penyuntingan melalui surat elektronik saja', 1),
(29, 3, 6, 'Menggunakan manajemen pengelolaan penyuntingan secara pencatatan manual saja', 0.5),
(30, 4, 7, 'Superspesialis, misalnya: taksonomi jamur, atau studi Jepang', 4),
(31, 4, 7, 'Spesialis, misalnya: fisiologi tumbuhan atau ekologi pesisir, atau studi Asia Timur', 3),
(32, 4, 7, 'Cabang ilmu, misalnya: botani atau studi wilayah ', 2),
(33, 4, 7, 'Disiplin ilmu, misalnya: biologi atau sosiologi ', 1),
(34, 4, 7, 'Bunga rampai dan kombinasi berbagai disiplin ilmu, misalnya: MIPA, sains dan keteknikan', 0),
(35, 4, 8, 'Internasional ', 6),
(36, 4, 8, 'Regional ', 4),
(37, 4, 8, 'Nasional ', 3),
(38, 4, 8, 'Kawasan ', 1),
(39, 4, 8, 'Lokal', 0),
(40, 4, 9, 'Memuat artikel yang berisi karya orisinal dan mempunyai kebaruan/memberikan kontribusi ilmiah tinggi', 6),
(41, 4, 9, 'Memuat artikel yang berisi karya orisinal dan mempunyai kebaruan/memberikan kontribusi ilmiah cukup', 4),
(42, 4, 9, 'Memuat artikel yang berisi karya orisinal dan mempunyai kebaruan/memberikankontribusi ilmiah rendah', 2),
(43, 4, 9, 'Memuat artikel yang berisi karya tidak orisinal dan/atau tidak mempunyai kebaruan/memberikan kontribusi ilmiah', 0),
(44, 4, 10, 'Sangat nyata', 3),
(45, 4, 10, 'Nyata', 2),
(48, 4, 10, 'Tidak nyata', 1),
(49, 4, 11, 'Tinggi (jumlah sitasi> 25) ', 5),
(50, 4, 11, 'Cukup (jumlah sitasi 11-25)', 4),
(51, 4, 11, 'Sedang (jumlah sitasi 6-10)', 3),
(52, 4, 11, 'Kurang (jumlah sitasi 1-5)', 1),
(53, 4, 11, 'Tidak berdampak (jumlah sitasi 0) ', 0),
(54, 4, 12, '> 80 % ', 4),
(55, 4, 12, '40-80 %', 2),
(56, 4, 12, '< 40 % ', 1),
(57, 4, 13, '> 80 %', 5),
(58, 4, 13, '40-80 % ', 3),
(59, 4, 13, '< 40 %', 1),
(60, 4, 14, 'Baik', 3),
(61, 4, 14, 'Cukup', 2),
(62, 4, 14, 'Kurang', 1),
(63, 4, 15, 'Baik', 3),
(64, 4, 15, 'Cukup', 2),
(65, 4, 15, 'Kurang', 1),
(69, 5, 16, 'Lugas dan Informatif', 1),
(70, 5, 16, 'Lugas tetapi kurang informatif atau sebaliknya', 0.5),
(71, 5, 16, 'Tidak lugas dan tidak informatif', 0),
(72, 5, 17, 'Lengkap dan konsisten', 1),
(73, 5, 17, 'Lengkap tetapi tidak konsisten', 0.5),
(74, 5, 17, 'Tidak lengkap dan tidak konsisten', 0),
(75, 5, 18, 'Abstrak yang jelas dan ringkas dalam Bahasa Inggris dan/atau Bahasa Indonesia', 2),
(76, 5, 18, 'Abstrak kurang jelas dan ringkas atau hanya dalam Bahasa Inggris atau dalam Bahasa Indonesia saja', 1),
(77, 5, 18, 'Abstrak tidak jelas dan bahasa tidak\r\nbaku', 0.5),
(78, 5, 19, 'Ada, konsisten dan mencerminkan konsep penting dalam artikel', 1),
(79, 5, 19, 'Ada tetapi kurang konsisten atau kurang mencerminkan konsep penting dalam artikel', 0.5),
(80, 5, 19, 'Tidak ada', 0),
(81, 5, 20, 'Lengkap dan bersistem baik', 1),
(82, 5, 20, 'Lengkap tetapi tidak bersistem baik', 0.5),
(83, 5, 20, 'Kurang lengkap dan tidak bersistem', 0),
(84, 5, 21, 'Informatif dan komplementer', 1),
(85, 5, 21, 'Kurang informatif atau komplementer', 0.5),
(86, 5, 21, 'Tak termanfaatkan', 0),
(87, 5, 22, 'Baku dan konsistendan menggunakan aplikasi pengutipan standar', 1),
(88, 5, 22, 'Baku dan konsisten tetapi tidak menggunakan aplikasi pengutipan standar', 0.5),
(89, 5, 22, 'Tidak baku dan tidak konsisten', 0),
(90, 5, 23, 'Baku dan konsisten dan menggunakan aplikasi pengutipan standar', 2),
(91, 5, 23, 'Baku dan konsisten, tetapi tidak menggunakan aplikasi pengutipan standar', 1),
(92, 5, 23, 'Tidak baku dan tidak konsisten', 0),
(93, 5, 24, 'Berbahasa Indonesia atau berbahasa resmi PBB yang baik dan benar', 2),
(94, 5, 24, 'Berbahasa Indonesia atau berbahasa resmi PBB yang cukup baik dan benar', 1),
(95, 5, 24, 'Berbahasa yang buruk', 0),
(96, 6, 25, 'Konsisten berukuran A4 (210x297 mm)', 1),
(97, 6, 25, 'Konsisten berukuran lainnya', 0.5),
(98, 6, 25, 'Tidak konsisten', 0),
(99, 6, 26, 'Konsisten antar artikel dan antar terbitan', 1),
(100, 6, 26, 'Kurang konsisten', 0.5),
(101, 6, 26, 'Tidak konsisten', 0),
(102, 6, 27, 'Konsisten antar artikel dan antar terbitan', 1),
(103, 6, 27, 'Kurang konsisten', 0.5),
(104, 6, 27, 'Tidak konsisten', 0),
(105, 6, 28, 'Versi daring: Konsisten dan berkualitas resolusi tinggi, atau Versi cetak: Konsisten, berkualitas tinggi dan dicetak di atas coated paper', 2),
(106, 6, 28, 'Versi daring: Konsisten dan berkualitas resolusi rendah, atau Versi cetak: Konsisten dan berkualitas sedang dan tidak tergolong coated paper', 1),
(107, 6, 28, 'Tidak konsisten', 0.5),
(108, 6, 29, '>= 500 halaman', 2),
(109, 6, 29, '201-499 halaman', 1),
(110, 6, 29, '100-200 halaman', 0.5),
(111, 6, 29, '<100 halaman', 0),
(112, 6, 30, 'Berciri khas, dan memberikan informasi yang jelas', 1),
(113, 6, 30, 'Tidak berciri khas', 0),
(114, 7, 31, '>80% terbitan sesuai dengan periode yang ditentukan', 2),
(115, 7, 31, '40-80 % terbitan sesuai dengan periode yang ditentukan', 1),
(116, 7, 31, '<40% terbitan sesuai dengan periode yang ditentukan', 0),
(117, 7, 32, 'Baku dan bersistem', 2),
(118, 7, 32, 'Tidak baku tetapi bersistem', 1),
(119, 7, 32, 'Tidak bersistem dan tidak baku', 0),
(120, 7, 33, 'Berurut dalam satu volume', 1),
(121, 7, 33, 'Tiap nomor dimulai dengan halaman baru', 0),
(122, 7, 34, 'Berindeks subjek dan berindeks pengarang yang terinci', 1),
(123, 7, 34, 'Berindeks subjek saja, atau berindeks pengarang saja', 0.5),
(124, 7, 34, 'Tidak berindeks', 0),
(125, 8, 35, '>50 kunjungan unik pelanggan rerata per hari untuk jurnal yang terbit secara daring', 4),
(126, 8, 35, '10-50 kunjungan unik pelanggan rerata per hari untuk jurnal yang terbit secara daring atau jumlah pelanggan >1000 eksemplar untuk jurnal yang masih terbit secara cetak', 2),
(127, 8, 35, '<10 kunjungan unik pelanggan rerata per hari untuk jurnal yang terbit secara daring atau jumlah pelanggan antara 401-1000 eksemplar untuk jurnal yang masih terbit secara cetak', 1),
(128, 8, 36, 'Tercantum di lembaga pengindeks internasional bereputasi tinggi', 5),
(129, 8, 36, 'Tercantum dalam lembaga pengindeks internasional bereputasi sedang', 3),
(130, 8, 36, 'Tercantum dalam lembaga pengindeks internasional bereputasi rendah', 1),
(131, 8, 37, 'Memiliki DOI tiap artikel', 2),
(132, 8, 37, 'Memiliki alamat laman yang permanen tiap artikel', 1),
(133, 8, 37, 'Tidak memiliki DOI ataupun alamat laman permanen', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_penilaian`
--

CREATE TABLE `tb_penilaian` (
  `id_penilaian` int(11) NOT NULL,
  `judul_paper` varchar(200) NOT NULL,
  `pengelola` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `nilai` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penilaian`
--

INSERT INTO `tb_penilaian` (`id_penilaian`, `judul_paper`, `pengelola`, `email`, `nilai`) VALUES
(1, 'jkl', 'jkl', 'jkl@9999.com', '[{"id_penilaian":"1","total_nilai":2},{"id_penilaian":"2","total_nilai":3},{"id_penilaian":"3","id_sub":["1","2","3","4","5","6"],"Nilai":{"1":"3","2":"1","3":"2","4":"1","5":"1","6":"2"},"total_nilai":10},{"id_penilaian":"4","id_sub":["7","8","9","10","11","12","13","14","15"],"Nilai":{"7":"3","8":"4","9":"4","10":"2","11":"4","12":"2","13":"3","14":"2","15":"2"},"total_nilai":26},{"id_penilaian":"5","id_sub":["16","17","18","19","20","21","22","23","24"],"Nilai":{"16":"0.5","17":"0.5","18":"1","19":"0.5","20":"0.5","21":"0.5","22":"0.5","23":"1","24":"1"},"total_nilai":6},{"id_penilaian":"6","id_sub":["25","26","27","28","29","30"],"Nilai":{"25":"0.5","26":"0.5","27":"0.5","28":"1","29":"1","30":"0"},"total_nilai":3.5},{"id_penilaian":"7","id_sub":["31","32","33","34"],"Nilai":{"31":"1","32":"1","33":"1","34":"0.5"},"total_nilai":3.5},{"id_penilaian":"8","id_sub":["35","36","37"],"Nilai":{"35":"2","36":"3","37":"1"},"total_nilai":6}]'),
(2, 'Jurnal Redha', 'Redha', 'redha@gmail.com', '[{"id_penilaian":"1","total_nilai":2},{"id_penilaian":"2","total_nilai":4},{"id_penilaian":"3","id_sub":["1","2","3","4","5","6"],"Nilai":{"1":"1","2":"2","3":"2","4":"1","5":"2","6":"1"},"total_nilai":9},{"id_penilaian":"4","id_sub":["7","8","9","10","11","12","13","14","15"],"Nilai":{"7":"2","8":"4","9":"6","10":"3","11":"3","12":"4","13":"1","14":"2","15":"3"},"total_nilai":28},{"id_penilaian":"5","id_sub":["16","17","18","19","20","21","22","23","24"],"Nilai":{"16":"0","17":"0.5","18":"0.5","19":"0.5","20":"0.5","21":"0","22":"0.5","23":"1","24":"1"},"total_nilai":4.5},{"id_penilaian":"6","id_sub":["25","26","27","28","29","30"],"Nilai":{"25":"0.5","26":"0.5","27":"0.5","28":"2","29":"0.5","30":"0"},"total_nilai":4},{"id_penilaian":"7","id_sub":["31","32","33","34"],"Nilai":{"31":"1","32":"1","33":"0","34":"1"},"total_nilai":3},{"id_penilaian":"8","id_sub":["35","36","37"],"Nilai":{"35":"2","36":"3","37":"2"},"total_nilai":7}]');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sub_unsur_penilaian`
--

CREATE TABLE `tb_sub_unsur_penilaian` (
  `id_sub_unsur` int(11) NOT NULL,
  `id_unsur_penilaian` int(11) NOT NULL,
  `sub_unsur_penilaian` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sub_unsur_penilaian`
--

INSERT INTO `tb_sub_unsur_penilaian` (`id_sub_unsur`, `id_unsur_penilaian`, `sub_unsur_penilaian`) VALUES
(1, 3, 'Pelibatan Mitra Bebestari'),
(2, 3, 'Mutu Penyuntingan Substansi'),
(3, 3, 'Kualifikasi Dewan Penyunting'),
(4, 3, 'Petunjuk Penulisan bagi Penulis'),
(5, 3, 'Mutu Penyuntingan Gaya dan Format'),
(6, 3, 'Manajemen Pengelolaan Terbitan Berkala Ilmiah'),
(7, 4, 'Cakupan Keilmuan'),
(8, 4, 'Aspirasi Wawasan'),
(9, 4, 'Kepioniran Ilmiah / Orisinalitas Karya'),
(10, 4, 'Makna Sumbangan bagi Kemajuan Ilmu'),
(11, 4, 'Dampak Ilmiah'),
(12, 4, 'Nisbah Sumber Acuan Primer berbanding Sumber lainnya'),
(13, 4, 'Derajat Kemutakhiran Pustaka Acuan'),
(14, 4, 'Analisis dan Sintesis'),
(15, 4, 'Penyimpulan dan Perampatan'),
(16, 5, 'Keefektifan Judul Artikel'),
(17, 5, 'Pencantuman Nama Penulis dan Lembaga Penulis'),
(18, 5, 'Abstrak'),
(19, 5, 'Kata Kunci'),
(20, 5, 'Sistematika Pembaban'),
(21, 5, 'Pemanfaatan Instrumen Pendukung'),
(22, 5, 'Cara Pengacuan dan Pengutipan'),
(23, 5, 'Penyusunan Daftar Pustaka'),
(24, 5, 'Peristilahan dan Kebahasaan'),
(25, 6, 'Ukuran Bidang Tulisan'),
(26, 6, 'Tata Letak'),
(27, 6, 'Tipografi'),
(28, 6, 'Resolusi Dokumen PDF (versi daring) atau Jenis Kertas (versi cetak)'),
(29, 6, 'Jumlah Halaman per Jilid atau Volume'),
(30, 6, 'Desain Tampilan Laman (Website) atau Desain Sampul'),
(31, 7, 'Jadwal Penerbitan'),
(32, 7, 'Tata Penomoran Penerbitan'),
(33, 7, 'Penomoran Halaman'),
(34, 7, 'Indeks Tiap Jilid atau Volume'),
(35, 8, 'Jumlah Kunjungan Unik Pelanggan'),
(36, 8, 'Pencantuman di Pengindeks Internasional Bereputasi'),
(37, 8, 'Alamat/Identitas Unik Artikel');

-- --------------------------------------------------------

--
-- Table structure for table `tb_unsur_penilaian`
--

CREATE TABLE `tb_unsur_penilaian` (
  `id_unsur` int(11) NOT NULL,
  `unsur_penilaian` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_unsur_penilaian`
--

INSERT INTO `tb_unsur_penilaian` (`id_unsur`, `unsur_penilaian`) VALUES
(1, 'Penamaan Terbitan Berkala Ilmiah'),
(2, 'Kelembagaan Penerbit'),
(3, 'Penyuntingan dan Manajemen Pengelolaan Terbitan'),
(4, 'Substansi Artikel'),
(5, 'Gaya Penulisan'),
(6, 'Penampilan'),
(7, 'Keberkalaan'),
(8, 'Penyebarluasan');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'adminjurnal', 'adminjurnal@gmail.com', '$2y$10$eyRAotW52kubLJ2IMMoiQObVBvn0D0b4AOIVJCVr5fAnD9w4yYE/a', 'lmZ6pFWo3dWHuCxu4qGjxHSylOdzapkZwuZF95yFY9vCPW0MsY3miSPOwGye', '2017-03-19 09:28:35', '2017-03-20 02:54:12'),
(2, 'test', 'test@gmail.com', '$2y$10$S8URViPe/fhNosYDj.nOduC3qcndIL/Nh1EqfGhHhQvLzqqP5dDCi', NULL, '2017-03-19 10:01:42', '2017-03-19 10:01:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `materis`
--
ALTER TABLE `materis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `tb_indikator_penilaian`
--
ALTER TABLE `tb_indikator_penilaian`
  ADD PRIMARY KEY (`id_indikator`);

--
-- Indexes for table `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
  ADD PRIMARY KEY (`id_penilaian`);

--
-- Indexes for table `tb_sub_unsur_penilaian`
--
ALTER TABLE `tb_sub_unsur_penilaian`
  ADD PRIMARY KEY (`id_sub_unsur`);

--
-- Indexes for table `tb_unsur_penilaian`
--
ALTER TABLE `tb_unsur_penilaian`
  ADD PRIMARY KEY (`id_unsur`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `materis`
--
ALTER TABLE `materis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_indikator_penilaian`
--
ALTER TABLE `tb_indikator_penilaian`
  MODIFY `id_indikator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
  MODIFY `id_penilaian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_sub_unsur_penilaian`
--
ALTER TABLE `tb_sub_unsur_penilaian`
  MODIFY `id_sub_unsur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tb_unsur_penilaian`
--
ALTER TABLE `tb_unsur_penilaian`
  MODIFY `id_unsur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
