<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Akreditasi Jurnal Untan</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="{{ asset('css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="{{ asset('img/logo-untan.png') }}" type="image/x-icon" />

    <!-- owl carousel css -->
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.css') }}" rel="stylesheet">
</head>

<body>
    <div id="all">
        <header>
            <!-- *** NAVBAR *** -->
            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header" style="padding-top:0.3%">
                            <div style="float:left;">
                            <a class="navbar-brand home" href="index.html">
                                <img src="{{ asset('img/logo-untan.png') }}" alt="Data Untan" class="hidden-xs hidden-sm">
                                <img src="{{ asset('img/logo-untan.png') }}" alt="Data Untan" class="visible-xs visible-sm">
                            </a>
                            <font style="padding-top:16px; float:right; line-height: 19px">
                                <font style="color:#C4DA20; font-weight: 500; font-size: 2.700rem;">AKREDITASI&nbsp;JURNAL</font><br>
                                <font style="padding-top:-20px; font-weight: 300; font-size: 1.600rem;">Universitas Tanjungpura</font>
                            </font>
                            </div>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>

                        <div class="navbar-collapse collapse" id="navigation">
                            <ul class="nav navbar-nav navbar-right">
                                <li class=""><a href="{{ url('/') }}">Pendaftaran</a></li>
                                <li class="dropdown active"><a href="{{ url('/history-penilaian') }}">Riwayat</a></li>
                                <li class="">
                                    <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                    </a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- *** NAVBAR END *** -->
        </header>

        <div class="home-carousel">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="col-md-12 ">
                    <font style="font-weight: 500; font-size: 2.200rem; color:white">FORM AKREDITASI JURNAL</font>
                    <p></p>
                </div>
            </div>
        </div><br>


        <div class="container">
            <div class="col-md-12">
                <div class="heading text-left" style="padding-top=100px">
                    <h4>Detail Penilaian User -> {{ $email }}</h4>
                </div>
                <div class="formfilterdata text-left" style="padding-top=100px">
                    <font style="color:white">Akreditasi terbitan berkala ilmiah terdiri atas 8 (delapan) unsur penilaian, yang merupakan kriteria untuk menentukan peringkat dan status akreditasi suatu terbitan berkala ilmiah.
                    Penamaan terbitan berkala ilmiah, kelembagaan penerbit, penyuntingan dan manajemen pengelolaan terbitan, substansi artikel, gaya penulisan, penampilan, keberkalaan dan penyebarluasan.</font><br>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td width="15%"><font style="color:#555564"><b>JUDUL JURNAL</b></font></td>
                            <td width="1%"><b>:</b></td>
                            <td>{{ $judul }}</td>
                        </tr>
                        <tr>
                            <td width="15%"><font style="color:#555564"><b>PENGELOLA</b></font></td>
                            <td width="1%"><b>:</b></td>
                            <td>{{ $pengelola }}</td>
                        </tr>
                        <tr>
                            <td width="15%"><font style="color:#555564"><b>ALAMAT EMAIL</b></font></td>
                            <td width="1%"><b>:</b></td>
                            <td>{{ $email }}</td>
                        </tr>
                        <tr>
                            <td width="15%"><font style="color:#555564"><b>TOTAL NILAI</b></font></td>
                            <td width="1%"><b>:</b></td>
                            <?php
                                $totalnilai = $detailnye[0]['total_nilai']+$detailnye[1]['total_nilai']+$detailnye[2]['total_nilai']+$detailnye[3]['total_nilai']+$detailnye[4]['total_nilai']+$detailnye[5]['total_nilai']+$detailnye[6]['total_nilai']+$detailnye[7]['total_nilai'];
                            ?>
                            <td><font style="color:green"><b>{{ $totalnilai }}</b></font></td>
                        </tr>
                    </table>
                </div>

                <div class="no-more-tables">
                    <table class="col-sm-12 table-bordered table-striped table-condensed">
                        <thead style="background-color=#F1S83F">
                            <tr>
                                <th width="5%" class="numeric"><center>NO</center></th>
                                <th class="numeric"><center>PENILAIAN</center></th>
                                <th class="numeric"><center>NILAI</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td align="center"><b>A</b></td>
                                <td><b>PENAMAAN TERBITAN BERKALA ILMIAH</b></td>
                                <td align="center">{{ $detailnye[0]['total_nilai'] }}</td>
                            </tr>
                            <tr>
                                <td align="center"><b>B</b></td>
                                <td><b>KELEMBAGAAN PENERBIT</b></td>
                                <td align="center">{{ $detailnye[1]['total_nilai'] }}</td>
                            </tr>
                            <tr>
                                <td rowspan="7" align="center" valign="top"><b>C</b></td>
                                <td><b>PENYUNTINGAN DAN MANAJEMEN PENGELOLAAN TERBITAN</b></td>
                                <td align="center"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;1. Pelibatan Mitra Bebestari</td>
                                <td align="center">{{ $detailnye[2]['Nilai'][1] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;2. Mutu Penyuntingan Substansi</td>
                                <td align="center">{{ $detailnye[2]['Nilai'][2] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;3. Kualifikasi Dewan Penyunting</td>
                                <td align="center">{{ $detailnye[2]['Nilai'][3] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;4. Petunjuk Penulisan bagi Penulis</td>
                                <td align="center">{{ $detailnye[2]['Nilai'][4] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;5. Mutu Penyuntingan Gaya dan Format</td>
                                <td align="center">{{ $detailnye[2]['Nilai'][5] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;6. Manajemen Pengelolaan Terbitan Berkala Ilmiah</td>
                                <td align="center">{{ $detailnye[2]['Nilai'][6] }}</td>
                            </tr>
                            <tr>
                                <td rowspan="10" align="center" valign="top"><b>D</b></td>
                                <td><b>SUBSTANSI ARTIKEL</b></td>
                                <td align="center"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;1. Cakupan Keilmuan</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][7] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;2. Aspirasi Wawasan</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][8] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;3. Kepioniran Ilmiah / Orisinalitas Karya</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][9] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;4. Makna Sumbangan bagi Kemajuan Ilmu</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][10] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;5. Dampak Ilmiah</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][11] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;6. Nisbah Sumber Acuan Primer berbanding Sumber lainnya</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][12] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;7. Derajat Kemutakhiran Pustaka Acuan</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][13] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;8. Analisis dan Sintesis</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][14] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;9. Penyimpulan dan Perampatan</td>
                                <td align="center">{{ $detailnye[3]['Nilai'][15] }}</td>
                            </tr>
                            <tr>
                                <td rowspan="10" align="center" valign="top"><b>E</b></td>
                                <td><b>GAYA PENULISAN</b></td>
                                <td align="center"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;1. Keefektifan Judul Artikel</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][16] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;2. Pencantuman Nama Penulis dan Lembaga Penulis</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][17] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;3. Abstrak</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][18] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;4. Kata Kunci</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][19] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;5. Sistematika Pembaban</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][20] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;6. Pemanfaatan Instrumen Pendukung</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][21] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;7. Cara Pengacuan dan Pengutipan</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][22] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;8. Penyusunan Daftar Pustaka</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][23] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;9. Peristilahan dan Kebahasaan</td>
                                <td align="center">{{ $detailnye[4]['Nilai'][24] }}</td>
                            </tr>
                            <tr>
                                <td rowspan="7" align="center" valign="top"><b>F</b></td>
                                <td><b>PENAMPILAN</b></td>
                                <td align="center"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;1. Ukuran Bidang Tulisan</td>
                                <td align="center">{{ $detailnye[5]['Nilai'][25] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;2. Tata Letak</td>
                                <td align="center">{{ $detailnye[5]['Nilai'][26] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;3. Tipografi</td>
                                <td align="center">{{ $detailnye[5]['Nilai'][27] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;4. Resolusi Dokumen PDF (versi daring) atau Jenis Kertas (versi cetak)</td>
                                <td align="center">{{ $detailnye[5]['Nilai'][28] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;5. Jumlah Halaman per Jilid atau Volume</td>
                                <td align="center">{{ $detailnye[5]['Nilai'][29] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;6. Desain Tampilan Laman (Website) atau Desain Sampul</td>
                                <td align="center">{{ $detailnye[5]['Nilai'][30] }}</td>
                            </tr>
                            <tr>
                                <td rowspan="5" align="center" valign="top"><b>G</b></td>
                                <td><b>KEBERKALAAN</b></td>
                                <td align="center"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;1. Jadwal Penerbitan</td>
                                <td align="center">{{ $detailnye[6]['Nilai'][31] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;2. Tata Penomoran Penerbitan</td>
                                <td align="center">{{ $detailnye[6]['Nilai'][32] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;3. Penomoran Halaman</td>
                                <td align="center">{{ $detailnye[6]['Nilai'][33] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;4. Indeks Tiap Jilid atau Volume</td>
                                <td align="center">{{ $detailnye[6]['Nilai'][34] }}</td>
                            </tr>
                            <tr>
                                <td rowspan="7" align="center" valign="top"><b>H</b></td>
                                <td><b>PENYEBARLUASAN</b></td>
                                <td align="center"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;1. Jumlah Kunjungan Unik Pelanggan</td>
                                <td align="center">{{ $detailnye[7]['Nilai'][35] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;2. Pencantuman di Pengindeks Internasional Bereputasi</td>
                                <td align="center">{{ $detailnye[7]['Nilai'][36] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;3. Alamat/Identitas Unik Artikel</td>
                                <td align="center">{{ $detailnye[7]['Nilai'][37] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- </div> -->
            </div>
        </div>
        <br><br><br>

        <!-- *** COPYRIGHT *** -->
        <div id="copyright">
            <div class="container">
                <div class="col-md-12">
                    <font style="color:#C4DA20; font-weight: 300; font-size: 1.800rem;">UPT. Teknologi Informasi dan Komunikasi</font><br>
                    <font>Form Akreditasi Jurnal &copy; 2017 Universitas Tanjungpura</font>
                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->
    </div>
    <!-- /#all -->

    <!-- #### JAVASCRIPT FILES ### -->
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>

    <script>
        window.jQuery || document.write('<script src="{{ asset("js/jquery-1.11.0.min.js") }}"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('js/front.js') }}"></script>
</body>
</html>
