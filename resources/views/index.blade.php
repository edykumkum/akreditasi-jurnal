<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Akreditasi Jurnal Untan</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="{{ asset('css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="{{ asset('img/logo-untan.png') }}" type="image/x-icon" />

    <!-- owl carousel css -->
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.css') }}" rel="stylesheet">
</head>

<body>
    <div id="all">
        <header>
            <!-- *** NAVBAR *** -->
            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header" style="padding-top:0.3%">
                            <div style="float:left;">
                            <a class="navbar-brand home" href="">
                                <img src="{{ asset('img/logo-untan.png') }}" alt="Data Untan" class="hidden-xs hidden-sm">
                                <img src="{{ asset('img/logo-untan.png') }}" alt="Data Untan" class="visible-xs visible-sm">
                            </a>
                            <font style="padding-top:16px; float:right; line-height: 19px">
                                <font style="color:#C4DA20; font-weight: 500; font-size: 2.700rem;">AKREDITASI&nbsp;JURNAL</font><br>
                                <font style="padding-top:-20px; font-weight: 300; font-size: 1.600rem;">Universitas Tanjungpura</font>
                            </font>
                            </div>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>

                        <div class="navbar-collapse collapse" id="navigation">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown active"><a href="{{ url('/') }}">Pendaftaran</a></li>
                                @if(Auth::check())
                                <li class="dropdown"><a href="{{ url('/history-penilaian') }}">Riwayat</a></li>
                                <li class="">
                                    <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                    Logout
                                    </a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- *** NAVBAR END *** -->
        </header>

        <div class="home-carousel">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="col-md-12 ">
                    <font style="font-weight: 500; font-size: 2.200rem; color:white">FORM AKREDITASI JURNAL</font>
                    <p></p>
                </div>
            </div>
        </div><br>

        <div class="container">
            <div class="col-md-12">
                <div class="heading text-left" style="padding-top=100px">
                    <h4>Pendaftaran</h4>
                </div>
                <form action="penilaian-1" class="form-horizontal" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <label class="control-label">Nama Jurnal</label><br>
                    <input type="text" class="form-control" name="judulpaper" placeholder="Ketikan judul jurnal dengan lengkap" / required><br>
                    <label class="control-label">Pengelola</label><br>
                    <input type="text" class="form-control" name="pengelola" placeholder="Ketikan nama pengelola dengan lengkap" / required><br>
                    <label class="control-label">Alamat Email</label><br>
                    <input type="email" class="form-control" name="email" placeholder="Ketikan alamat email dengan lengkap" / required><br>
                    <input type="hidden" name="nopage" value="0" />
                    <center><button type="submit" class="btn btn-success"><b>Daftar</b></button></center>
                </form> <br><br><br>
            </div>
        </div>
        <!-- *** COPYRIGHT *** -->
        <div id="copyright" style="position: absolute; left: 0; bottom: 0; width: 100%;">
            <div class="container">
                <div class="col-md-12">
                    <font style="color:#C4DA20; font-weight: 300; font-size: 1.800rem;">UPT. Teknologi Informasi dan Komunikasi</font><br>
                    <font>Form Akreditasi Jurnal &copy; 2017 Universitas Tanjungpura</font>
                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->
    </div>
    <!-- /#all -->

    <!-- #### JAVASCRIPT FILES ### -->
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>

    <script>
        window.jQuery || document.write('<script src="{{ asset("js/jquery-1.11.0.min.js") }}"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('js/front.js') }}"></script>
</body>
</html>
