<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Akreditasi Jurnal Untan</title>
        <link rel="shortcut icon" href="{{ asset('img/logo-untan.png') }}" type="image/x-icon" />
        <link rel="stylesheet" href="{{ asset('css/loginstyle.css') }}"/>
    </head>
    <body>
        <div id="wrap">
            <div id="content">
                <form id="sign-in" class="active" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <a class="navbar-brand home" href="index.html">
                        <img src="{{ asset('img/logo-untan.png') }}" alt="Data Untan" class="hidden-xs hidden-sm">
                    </a>
                        <font style="color:#C4DA20; font-weight: 500; font-size: 1.000rem;">AKREDITASI&nbsp;JURNAL</font><br>
                        <font style="font-weight: 300; font-size: 1.000rem;">Universitas Tanjungpura</font>
                    <!-- </font><br> --><br><br>
                    <input type="text" id="username" name="username" placeholder="pengguna"/>
                    <input type="password" id="password" name="password" placeholder="sandi"/>
                    <input type="submit" value="Masuk"/>
                    <font style="font-weight: 200; font-size: 0.700rem;">Form Akreditasi Jurnal <br>© 2017 Universitas Tanjungpura</font>
                    <!-- <a class="btn btn-link" href="{{ url('/password/reset') }}">
                        Lupa Sandi Anda?
                    </a> -->
                </form>
                <!-- <form id="sign-up" action="/proses-dosen-registrasi-akun" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <h2>Registrasi</h2>
                    <input type="text" id="no_nidn" name="no_nidn" placeholder="nomor nidn"/>
                    <input type="text" placeholder="username"/>
                    <input type="password" placeholder="password"/>
                    <input type="password" placeholder="password confirm"/>
                    <input type="submit" value="Selanjutnya"/>
                    <a href="#" id="to-in">Sudah punya akun?</a>
                </form> -->
            </div>
        </div>

        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/zepto/1.1.4/zepto.min.js"></script>
        <script>
            Zepto(function($){
                $('a').on('click', function(){
                    if($(this).attr("id") == "to-up") {
                        $("#sign-in").removeClass("active");
                        $("#sign-up").addClass("active");
                    } else {
                        $("#sign-up").removeClass("active");
                        $("#sign-in").addClass("active");
                    }
                });
            })
        </script> -->
    </body>
</html>
