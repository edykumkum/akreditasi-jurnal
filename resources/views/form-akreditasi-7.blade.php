<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Akreditasi Jurnal Untan</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="{{ asset('css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="{{ asset('img/logo-untan.png') }}" type="image/x-icon" />

    <!-- owl carousel css -->
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.css') }}" rel="stylesheet">
</head>

<body>
    <div id="all">
        <header>
            <!-- *** NAVBAR *** -->
            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header" style="padding-top:0.3%">
                            <div style="float:left;">
                            <a class="navbar-brand home" href="index.html">
                                <img src="{{ asset('img/logo-untan.png') }}" alt="Data Untan" class="hidden-xs hidden-sm">
                                <img src="{{ asset('img/logo-untan.png') }}" alt="Data Untan" class="visible-xs visible-sm">
                            </a>
                            <font style="padding-top:16px; float:right; line-height: 19px">
                                <font style="color:#C4DA20; font-weight: 500; font-size: 2.700rem;">AKREDITASI&nbsp;JURNAL</font><br>
                                <font style="padding-top:-20px; font-weight: 300; font-size: 1.600rem;">Universitas Tanjungpura</font>
                            </font>
                            </div>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>

                        <div class="navbar-collapse collapse" id="navigation">
                            <ul class="nav navbar-nav navbar-right">
                              <li class="dropdown active"><a href="{{ url('/') }}">Pendaftaran</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- *** NAVBAR END *** -->
        </header>

        <div class="home-carousel">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="col-md-12 ">
                    <font style="font-weight: 500; font-size: 2.200rem; color:white">FORM AKREDITASI JURNAL</font>
                    <p></p>
                </div>
            </div>
        </div><br>


        <div class="container">
            <div class="col-md-12">
                <div class="heading text-left" style="padding-top=100px">
                    <h4>Penilaian</h4>
                </div>
                <div class="formfilterdata text-left" style="padding-top=100px">
                    <font style="color:white">Akreditasi terbitan berkala ilmiah terdiri atas 8 (delapan) unsur penilaian, yang merupakan kriteria untuk menentukan peringkat dan status akreditasi suatu terbitan berkala ilmiah.
                    Penamaan terbitan berkala ilmiah, kelembagaan penerbit, penyuntingan dan manajemen pengelolaan terbitan, substansi artikel, gaya penulisan, penampilan, keberkalaan dan penyebarluasan.</font><br>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td width="15%"><font style="color:#555564"><b>JUDUL JURNAL</b></font></td>
                            <td width="1%"><b>:</b></td>
                            <td>{{ $judulpaper }}</td>
                        </tr>
                        <tr>
                            <td width="15%"><font style="color:#555564"><b>PENGELOLA</b></font></td>
                            <td width="1%"><b>:</b></td>
                            <td>{{ $pengelola }}</td>
                        </tr>
                        <tr>
                            <td width="15%"><font style="color:#555564"><b>ALAMAT EMAIL</b></font></td>
                            <td width="1%"><b>:</b></td>
                            <td>{{ $email }}</td>
                        </tr>
                    </table>
                </div>

                <div class="no-more-tables">
                    <table class="col-sm-12 table-bordered table-striped table-condensed cf">
                        <tbody>
                            <tr>
                                <td style="vertical-align:top" width="5%" data-title="Indikator" class="numeric"><center><b><font style="color:#20616D">G</font></b></center></td>
                                <td data-title="Indikator" class="string"><font style="color:#20616D"><b>{{ $penilaian->unsur_penilaian }}</b></font></td>
                            </tr>
                            <tr>
                                <?php $header = null; ?>
                                <td style="vertical-align:top" width="5%" data-title="Indikator" class="numeric"></td>

                                <td data-title="Indikator" class="string">
                                    <form action="penilaian-8" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="total_skor_lama" value="{{ $total_skor_baru }}" />
                                        @foreach($isi as $dataindikator)
                                            <input type="hidden" name="idunsur_penilaian" value="{{ $dataindikator->id_unsur_penilaian }}" />
                                            @if ($header != $dataindikator->sub_unsur_penilaian)
                                                 <b><br><font style="color:#20616D">-&nbsp; {{ $dataindikator->sub_unsur_penilaian }}</font></b><br>
                                                 <input type="hidden" name="id_subunsur_penilaian[]" value="{{ $dataindikator->id_sub_unsur_penilaian }}" />
                                                 <?php $header = $dataindikator->sub_unsur_penilaian ?>
                                            @endif
                                            <div class="btn-group btn-group-vertical" data-toggle="buttons">
                                                <label class="btn">
                                                    <input value="{{ $dataindikator->nilai }}" type="radio" name='indikator[{{ $dataindikator->id_sub_unsur_penilaian }}]' required><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i><span>&nbsp;{{ $dataindikator->indikator }}</span>
                                                </label>
                                            </div><br>
                                        @endforeach
                                        <input type="hidden" name="nopage" value="7" />
                                        <input type="hidden" name="judulpaper" value="{{ $judulpaper }}" />
                                        <input type="hidden" name="pengelola" value="{{ $pengelola }}" />
                                        <input type="hidden" name="email" value="{{ $email }}" />
                                        <input type="hidden" name="array_penilaian" value="{{ $array_penilaians }}" />
                                        <table width="100%">
                                            <tr>
                                                <td align="right"><br><button type="submit" id="simpan" name="simpan" class="btn btn-success"><b>Simpan</b></button><br></td>
                                                <td>&nbsp;</td>
                                                <td align="left"><br><button type="submit" id="lanjut" name="lanjut" class="btn btn-primary"><b>Lanjut</b></button><br></td>
                                            </tr>
                                        </table><br>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br><br><br>

        <!-- *** COPYRIGHT *** -->
        <div id="copyright">
            <div class="container">
                <div class="col-md-12">
                    <font style="color:#C4DA20; font-weight: 300; font-size: 1.800rem;">UPT. Teknologi Informasi dan Komunikasi</font><br>
                    <font>Form Akreditasi Jurnal &copy; 2017 Universitas Tanjungpura</font>
                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->
    </div>
    <!-- /#all -->

    <!-- #### JAVASCRIPT FILES ### -->
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>

    <script>
        window.jQuery || document.write('<script src="{{ asset("js/jquery-1.11.0.min.js") }}"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('js/front.js') }}"></script>
</body>
</html>
